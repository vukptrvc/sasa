<?php


session_start();

$errors = [];
$oldValues = [];


$oldValues['name'] = $_POST['name'] ?? '';
$oldValues['email'] = $_POST['email'] ?? '';
$oldValues['message'] = $_POST['message'] ?? '';


require 'Validators/emptyValidator.php';
require 'Validators/emailValidator.php';
require 'Validators/minValidator.php';
require 'Validators/maxValidator.php';

if (empty($errors)) {
    $_SESSION['mail_sent'] = true;
}


$_SESSION['errors'] = $errors;
$_SESSION['old_values'] = $oldValues;


header("Location: index.php");
exit();
?>
