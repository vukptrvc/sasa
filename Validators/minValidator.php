<?php

class MinValidator {
    public static function validate($string, $minLength, $maxLength) {
        $trimmedString = trim($string);
        $length = strlen($trimmedString);
        return ($length > $minLength && $length < $maxLength);
    }
}

if (MinValidator::validate($_POST['name'], 0, 3)) {
    $errors[] = "Name can't be shorter than 3 characters";
}

if (MinValidator::validate($_POST['message'], 0, 20)) {
    $errors[]= "Message too short! Min number of characters is 20";
}