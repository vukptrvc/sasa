<?php 

class MaxValidator {
    public static function validate($string, $maxLength) {
        $trimmedString = trim($string);
        $length = strlen($trimmedString);
        return ($length > $maxLength);
    }
}

if (MaxValidator::validate($_POST['name'], 15)) {
    $errors[] = "Name can't be longer than 15 characters";
}

if (MaxValidator::validate($_POST['message'], 1000)) {
    $errors[]= "Message too long! Max number of characters is 1000";
}