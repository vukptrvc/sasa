<?php

class EmptyValidator {
    public static function validate ($string) {
        return empty($string);
    }
}

if (EmptyValidator::validate($_POST['name'])) {
    $errors[] = "Name is required";
}

if (EmptyValidator::validate($_POST['message'])) {
    $errors[] = "Message is required";
}

if (EmptyValidator::validate($_POST['email'])) {
    $errors[] = "Email is required";
}