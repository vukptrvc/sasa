<?php

class EmailValidator {
    public static function validate ($value) {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}

if (!empty($_POST['email']) && !EmailValidator::validate($_POST["email"])) {
    $errors[] = "Invalid email format";
}
