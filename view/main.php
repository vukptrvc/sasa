<main class="bg-gray-200 flex-grow p-4" style="height: 75%; width: 100%;">
    <h1 class="text-xl">If you have any questions, you can send them here:</h1>
    <form action="contact.php" method="post">
        <fieldset class="border border-black w-500px h-400px p-2 m-8">
            <legend>Contact Us</legend>
            
            <div class="mt-4">
                <label for="name">Your name:</label>
                <input type="text" id="name" name="name"  class="border border-black" value="<?= isset($oldValues['name']) ? $oldValues['name'] : '' ?>">
            </div>

            <div class="mt-4">
                <label for="email">Your email:</label>
                <input type="email" id="email" name="email" class="border border-black" value="<?= isset($oldValues['email']) ? $oldValues['email'] : '' ?>">
            </div>
            
            <div class="mt-4 flex flex-col">
                <label for="message">Your message:</label>
                <textarea name="message" id="message" cols="30" rows="10" class="border border-black"><?= isset($oldValues['message']) ? $oldValues['message'] : '' ?></textarea>
            </div>

            <ul>
                <?php if (!empty($errors)) : ?>
                    <?php foreach ($errors as $error) :  ?>
                        <li class="text-red-500"> <?= $error ?> </li>
                    <?php endforeach; ?>
                <?php endif ?>
            </ul>

            <?php if ($mailSent): ?>
                <p class="text-green-500">Mail sent!</p>
            <?php endif; ?>

            <input type="submit" value="send" id="submit" name="submit" class="mt-6 mb-2 py-2 px-4 rounded-full bg-blue-500 cursor-pointer hover:bg-blue-300 text-white">
        </fieldset>
    </form>
</main>