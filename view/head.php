<?php
session_start();
$_SESSION['name'] = 'Vuk Learning PHP';

$errors = $_SESSION['errors'] ?? [];
$oldValues = $_SESSION['old_values'] ??  [];
$mailSent = $_SESSION['mail_sent'] ?? false;

unset($_SESSION['errors']);
unset($_SESSION['old_values']);
unset($_SESSION['mail_sent']);
?>
    
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="flex flex-col h-screen">